/*!

This module exposes a re-implementation of the algorithm described in:

1. Scaramuzza, D., Martinelli, A. and Siegwart, R.. "A Flexible Technique for Accurate Omnidirectional Camera Calibration and Structure from Motion", Proceedings of IEEE International Conference of Vision Systems (ICVS'06), New York, January 5-7, 2006.
2. Scaramuzza, D., Martinelli, A. and Siegwart, R.. "A Toolbox for Easy Calibrating Omnidirectional Cameras", Proceedings to IEEE International Conference on Intelligent Robots and Systems (IROS 2006), Beijing China, October 7-15, 2006.
3. Rufli, M., Scaramuzza, D., and Siegwart, R., Automatic Detection of Checkerboards on Blurred and Distorted Images,Proceedings of the IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS 2008), Nice, France, September 2008.

*/
use core::{f32, i32};

use crate::{Checkerboard, CheckerboardSpecification, Corner};
use image::RgbImage;
use nalgebra as na;
use opencv::core::{Point, Rect, Scalar, Size, Vec2f, Vec2i, Vec4, BORDER_CONSTANT, LINE_8};
use opencv::imgproc::{
    adaptive_threshold, approx_poly_dp, contour_area, convex_hull, cvt_color, dilate,
    find_contours_with_hierarchy, get_structuring_element, is_contour_convex,
    morphology_default_border_value, rectangle, ADAPTIVE_THRESH_GAUSSIAN_C, CHAIN_APPROX_SIMPLE,
    COLOR_RGB2GRAY, MORPH_CROSS, MORPH_RECT, RETR_CCOMP, THRESH_BINARY,
};
use opencv::prelude::*;
use opencv::types::VectorOfMat;
use std::collections::HashSet;

/// A Corner represents a corner within a checkerboard.,
/// `ix_x` and `ix_y` are its index within the checkerboard (from top left to bottom right).
/// `pos_x` and `pos_y` are its position within the frame (from top left to bottom right).
#[derive(Debug, Clone)]
struct CornerCandidate {
    ix: na::Point2<i32>,
    pos: na::Point2<f32>,
    needs_neighbour: bool,
}

impl Into<Corner> for CornerCandidate {
    fn into(self) -> Corner {
        Corner {
            ix: [self.ix.x, self.ix.y],
            pos: [self.pos.x, self.pos.y],
        }
    }
}

/// Quadrangle contains information about a checkerboard quadrangle.
#[derive(Debug, Clone)]
struct QuadrangleCandidate {
    group: Option<u32>,
    edge_length: f32,
    corners: [CornerCandidate; 4],
    neighbors_ix: [Option<usize>; 4],
    labeled: bool,
}

impl QuadrangleCandidate {
    fn neighbor_count(&self) -> usize {
        self.neighbors_ix.iter().filter(|n| n.is_some()).count()
    }
}

fn dilate_cross_rect(img: &mut Mat, dilations: u32) {
    let kernel1 = get_structuring_element(
        MORPH_CROSS,
        Size {
            width: 3,
            height: 3,
        },
        Point { x: 1, y: 1 },
    )
    .unwrap();
    let kernel2 = get_structuring_element(
        MORPH_RECT,
        Size {
            width: 3,
            height: 3,
        },
        Point { x: 1, y: 1 },
    )
    .unwrap();

    {
        //
        let mut dilation_counter = 0;
        // Helper closure to abbreviate the call to the dilation function.
        let mut dilate = |kernel| {
            dilate(
                &img.clone().unwrap(),
                img,
                kernel,
                Point { x: -1, y: -1 },
                1,
                BORDER_CONSTANT,
                morphology_default_border_value().unwrap(),
            )
            .unwrap();
        };
        // Alternate the two kernels until the image is dilated.
        loop {
            if dilation_counter < dilations {
                dilate(&kernel1);
            } else {
                break;
            }
            dilation_counter += 1;

            if dilation_counter < dilations {
                dilate(&kernel2);
            } else {
                break;
            }
            dilation_counter += 1;
        }
    }
}

fn whiten_border(img: &mut Mat) {
    rectangle(
        img,
        Rect {
            x: 0,
            y: 0,
            width: img.cols().unwrap(),
            height: img.rows().unwrap(),
        },
        Scalar::new(255.0, 255.0, 255.0, 0.0),
        3,
        LINE_8,
        0,
    )
    .unwrap();
}

/// Helper method that converts an image  an `image::RgbImage` to a `opencv::Mat`.
fn image_to_mat(image: &RgbImage) -> Mat {
    let samples = image.as_flat_samples();
    let mat = Mat::from_slice(samples.as_slice()).unwrap();
    mat.reshape(3, image.height() as i32).unwrap()
}

/// Detects the specified checkerboard in the given image using the Rufli et al. 2008 algorithm.
pub fn detect_checkerboard(
    image: &RgbImage,
    checker_spec: &CheckerboardSpecification,
) -> Option<Checkerboard> {
    let mut max_dilation_run_ix: Option<u32> = None;
    let min_dilations = 1;
    let max_dilations = 6;

    let mut input_img = image_to_mat(image);

    let cols = input_img.cols().unwrap();
    let rows = input_img.rows().unwrap();

    // image normalisation
    {
        let mut norm_img = Mat::default();
        cvt_color(&input_img, &mut norm_img, COLOR_RGB2GRAY, 0).unwrap();
        input_img = norm_img;
    }

    // image thresholding
    let mut thresh_img_save = Mat::default();
    let mut thresh_img = Mat::default();
    {
        // Ensure block_size is odd.
        let block_size = (rows.min(cols) / 5) | 1;
        adaptive_threshold(
            &input_img,
            &mut thresh_img,
            255.0,
            ADAPTIVE_THRESH_GAUSSIAN_C,
            THRESH_BINARY,
            block_size,
            0.0,
        )
        .unwrap();
        thresh_img.copy_to(&mut thresh_img_save).unwrap();
    }

    let mut output_quads: Vec<QuadrangleCandidate> = Vec::default();
    let mut output_quad_group: Vec<usize> = Vec::default();
    for dilations in min_dilations..=max_dilations {
        // restore image
        thresh_img_save.copy_to(&mut thresh_img).unwrap();

        dilate_cross_rect(&mut thresh_img, dilations);
        whiten_border(&mut thresh_img);

        let mut new_quads = generate_quads(&mut thresh_img, dilations, true).unwrap();
        if new_quads.is_empty() {
            continue;
        }
        find_quad_neighbors(&mut new_quads, dilations);

        for group_ix in 0.. {
            let mut quad_group = find_connected_quads(&mut new_quads, group_ix, dilations);
            if quad_group.is_empty() {
                break;
            }
            clean_found_connected_quads(&mut new_quads, &mut quad_group, &checker_spec);
            if quad_group.len() >= output_quad_group.len() {
                label_quad_group(&mut new_quads, &quad_group, &checker_spec, true);
                max_dilation_run_ix = Some(dilations);
                output_quad_group = quad_group;
                output_quads = new_quads.clone()
            }
        }
    }
    match validate_grid(&output_quads, &output_quad_group, &checker_spec) {
        GridValidationResult::Error(msg) => {
            eprintln!("{}", msg);
            return None;
        }
        GridValidationResult::FullGrid => {
            let quads: Vec<QuadrangleCandidate> = output_quad_group
                .iter()
                .map(|&ix| output_quads[ix].clone())
                .collect();
            return Some(Checkerboard {
                corners: quads_to_corners(quads.iter()),
                spec: checker_spec.clone(),
            });
        }
        GridValidationResult::PartialGrid => {
            // continue with step 2},
        }
    };

    // PART 2: AUGMENT LARGEST PATTERN
    for dilations in (min_dilations..=max_dilations).rev() {
        // restore image
        thresh_img_save.copy_to(&mut thresh_img).unwrap();

        dilate_cross_rect(&mut thresh_img, dilations);
        whiten_border(&mut thresh_img);

        let mut new_quads = generate_quads(&mut thresh_img, dilations, false).unwrap();
        if new_quads.is_empty() {
            continue;
        }

        while let AugmentationResult::SUCCESS = augment_best_run(
            &mut output_quads,
            &mut output_quad_group,
            &mut new_quads,
            max_dilation_run_ix.unwrap(),
            dilations,
        ) {
            label_quad_group(&mut output_quads, &output_quad_group, &checker_spec, false);
            match validate_grid(&output_quads, &output_quad_group, &checker_spec) {
                GridValidationResult::PartialGrid => continue,
                GridValidationResult::Error(msg) => {
                    eprintln!("{}", msg);
                    continue;
                }
                GridValidationResult::FullGrid => break,
            };
        }
    }

    Some(Checkerboard {
        corners: quads_to_corners(output_quads.iter()),
        spec: checker_spec.clone(),
    })
}

/// Collect the uniquee corners form the vector of quads.
fn quads_to_corners<'a>(quads: impl Iterator<Item = &'a QuadrangleCandidate>) -> Vec<Corner> {
    let mut seen = HashSet::new();
    let mut corners = Vec::new();

    for quad in quads {
        for corner in &quad.corners {
            let corner: Corner = corner.clone().into();
            if !seen.contains(&corner.ix) {
                seen.insert(corner.ix);
                corners.push(corner);
            }
        }
    }
    corners
}

#[derive(Debug, Clone)]
enum QuadGeneratorError {
    InvalidInputImage(&'static str),
}

fn generate_quads(
    image: &mut Mat,
    _dilation: u32,
    first_run: bool,
) -> Result<Vec<QuadrangleCandidate>, QuadGeneratorError> {
    let width = match image.cols() {
        Ok(value) => value,
        Err(_) => return Err(QuadGeneratorError::InvalidInputImage("Could not get width")),
    };
    let height = match image.rows() {
        Ok(value) => value,
        Err(_) => {
            return Err(QuadGeneratorError::InvalidInputImage(
                "Could not get height",
            ))
        }
    };

    // Empirical value from the cpp code
    let min_size = (f64::from(width) * f64::from(height) * 0.03 * 0.01 * 0.92 * 0.1).round();

    let mut contours: VectorOfMat = VectorOfMat::new();
    let mut hierarchy: Mat = Mat::new().unwrap();
    let mut board = None;
    find_contours_with_hierarchy(
        image,
        &mut contours,
        &mut hierarchy,
        RETR_CCOMP,
        CHAIN_APPROX_SIMPLE,
        Point { x: 0, y: 0 },
    )
    .unwrap();

    let get_parent = |ix| {
        let h_data: Vec4<i32> = *hierarchy.at_nd(&[0, ix as i32]).unwrap();
        h_data[3]
    };

    let is_hole = |ix| get_parent(ix) != -1;

    let mut counters = vec![0; contours.len()];

    let mut root: Vec<(usize, Mat)> = Vec::default();

    for (src_contour_ix, src_contour) in contours.iter().enumerate() {
        let area = contour_area(&src_contour, false).unwrap();

        let mut dst_contour = Mat::default();
        if is_hole(src_contour_ix) && area >= min_size {
            let min_approx_level = 2;
            let max_approx_level = if first_run { 3 } else { 7 };
            for approx_level in min_approx_level..=max_approx_level {
                let mut temp = Mat::default();
                approx_poly_dp(&src_contour, &mut temp, f64::from(approx_level), true).unwrap();
                approx_poly_dp(&temp, &mut dst_contour, f64::from(approx_level), true).unwrap();

                if dst_contour.total().unwrap() == 4 {
                    break;
                }
            }
            if dst_contour.total().unwrap() == 4 && is_contour_convex(&dst_contour).unwrap() {
                let parent = get_parent(src_contour_ix);
                debug_assert_ne!(parent, -1);
                let parent = parent as usize;

                counters[parent] += 1;

                if board.is_none() || counters[board.unwrap()] < counters[parent] {
                    board = Some(parent);
                }
                root.push((parent, dst_contour.clone().unwrap()));
            }
        }
    }
    Ok(root
        .into_iter()
        .filter_map(|(parent, contour)| {
            assert_eq!(contour.total().unwrap(), 4);

            if parent != board.unwrap() {
                return None;
            }

            let contour_at = |ix| {
                let v: Vec2i = *contour.at(ix).unwrap();
                na::Point2::new(v[0] as f32, v[1] as f32)
            };

            let corner_positions: [na::Point2<f32>; 4] =
                [contour_at(0), contour_at(1), contour_at(2), contour_at(3)];

            let edge_length = {
                let mut edge_length = f32::MAX;
                for i in 0..corner_positions.len() {
                    let dx = corner_positions[i].x
                        - corner_positions[(i + 1) % corner_positions.len()].x;
                    let dy = corner_positions[i].y
                        - corner_positions[(i + 1) % corner_positions.len()].y;
                    let d = dx * dx + dy * dy;
                    if edge_length > d {
                        edge_length = d;
                    }
                }
                edge_length
            };

            let corners: Vec<CornerCandidate> = corner_positions
                .iter()
                .map(|&pos| CornerCandidate {
                    ix: na::Point2::new(0, 0),
                    pos,
                    needs_neighbour: true,
                })
                .collect();

            Some(QuadrangleCandidate {
                group: None,
                neighbors_ix: [None, None, None, None],
                edge_length,
                labeled: false,
                corners: [
                    corners[0].clone(),
                    corners[1].clone(),
                    corners[2].clone(),
                    corners[3].clone(),
                ],
            })
        })
        .collect())
}

fn cross_2d(a: na::Vector2<f32>, b: na::Vector2<f32>) -> f32 {
    a[0] * b[1] - b[0] * a[1]
}

fn check_shared_corner_quadrant(
    q1: &QuadrangleCandidate,
    c1_ix: usize,
    q2: &QuadrangleCandidate,
    c2_ix: usize,
) -> bool {
    let xy1 = q1.corners[c1_ix].pos.coords + q1.corners[(c1_ix + 1) % 4].pos.coords / 2.0;
    let xy2 = q1.corners[(c1_ix + 2) % 4].pos.coords + q1.corners[(c1_ix + 3) % 4].pos.coords / 2.0;
    let xy3 = q1.corners[c1_ix].pos.coords + q1.corners[(c1_ix + 3) % 4].pos.coords / 2.0;
    let xy4 = q1.corners[(c1_ix + 1) % 4].pos.coords + q1.corners[(c1_ix + 2) % 4].pos.coords / 2.0;

    let ab1 = xy1 - xy2;

    let cd11 = q1.corners[c1_ix].pos.coords - xy2;
    let cd12 = q2.corners[c2_ix].pos.coords - xy2;

    let sign11: f32 = cross_2d(ab1, cd11);
    let sign12: f32 = cross_2d(ab1, cd12);

    let ab2 = xy3 - xy4;

    let cd21 = q1.corners[c1_ix].pos.coords - xy4;
    let cd22 = q2.corners[c2_ix].pos.coords - xy4;

    let sign21: f32 = cross_2d(ab2, cd21);
    let sign22: f32 = cross_2d(ab2, cd22);

    let cd13 = q2.corners[(c2_ix + 2) % 4].pos.coords - xy2;
    let cd23 = q2.corners[(c2_ix + 2) % 4].pos.coords - xy4;

    let sign13: f32 = cross_2d(ab1, cd13);
    let sign23: f32 = cross_2d(ab2, cd23);

    let uv1 = 0.5 * (q2.corners[c2_ix].pos.coords + q2.corners[(c2_ix + 1) % 4].pos.coords);
    let uv2 =
        0.5 * (q2.corners[(c2_ix + 2) % 4].pos.coords + q2.corners[(c2_ix + 3) % 4].pos.coords);
    let uv3 = 0.5 * (q2.corners[(c2_ix) % 4].pos.coords + q2.corners[(c2_ix + 3) % 4].pos.coords);
    let uv4 =
        0.5 * (q2.corners[(c2_ix + 1) % 4].pos.coords + q2.corners[(c2_ix + 2) % 4].pos.coords);

    let ab3 = uv1 - uv2;

    let cd31 = q1.corners[c1_ix].pos.coords - uv2;

    let cd32 = q2.corners[c2_ix].pos.coords - uv2;

    let sign31: f32 = cross_2d(ab3, cd31);
    let sign32: f32 = cross_2d(ab3, cd32);

    let ab4 = uv3 - uv4;

    let cd41 = q1.corners[c1_ix].pos.coords - uv4;

    let cd42 = q2.corners[c2_ix].pos.coords - uv4;

    let sign41: f32 = cross_2d(ab4, cd41);
    let sign42: f32 = cross_2d(ab4, cd42);

    let cd33 = q1.corners[(c1_ix + 2) % 4].pos.coords - uv2;

    let cd43 = q1.corners[(c1_ix + 2) % 4].pos.coords - uv4;

    let sign33: f32 = cross_2d(ab3, cd33);
    let sign43: f32 = cross_2d(ab4, cd43);

    ((sign11 < 0.0 && sign12 < 0.0) || (sign11 > 0.0 && sign12 > 0.0))
        && ((sign21 < 0.0 && sign22 < 0.0) || (sign21 > 0.0 && sign22 > 0.0))
        && ((sign31 < 0.0 && sign32 < 0.0) || (sign31 > 0.0 && sign32 > 0.0))
        && ((sign41 < 0.0 && sign42 < 0.0) || (sign41 > 0.0 && sign42 > 0.0))
        && ((sign11 < 0.0 && sign13 < 0.0) || (sign11 > 0.0 && sign13 > 0.0))
        && ((sign21 < 0.0 && sign23 < 0.0) || (sign21 > 0.0 && sign23 > 0.0))
        && ((sign31 < 0.0 && sign33 < 0.0) || (sign31 > 0.0 && sign33 > 0.0))
        && ((sign41 < 0.0 && sign43 < 0.0) || (sign41 > 0.0 && sign43 > 0.0))
}

fn find_quad_neighbors(quads: &mut [QuadrangleCandidate], dilations: u32) {
    let thresh_dilation = ((2 * dilations + 3) * (2 * dilations + 3) * 2) as f32;
    'quad_loop: for quad_ix in 0..quads.len() {
        for corner_this_ix in 0..4 {
            let mut min_distance = f32::MAX;
            let mut closest_quad_ix: Option<usize> = None;
            let mut closest_corner_ix: Option<usize> = None;

            let pt = quads[quad_ix].corners[corner_this_ix].pos;

            // Find the overall closest corner.
            for other_ix in 0..quads.len() {
                if quad_ix == other_ix {
                    continue;
                }
                for corner_other_ix in 0..4 {
                    if quads[other_ix].neighbors_ix[corner_other_ix].is_some() {
                        continue;
                    }
                    let corner_other = &quads[other_ix].corners[corner_other_ix];

                    let dxy = pt.coords - corner_other.pos.coords;
                    let dist_sq = dxy.magnitude_squared();

                    if dist_sq < min_distance
                        && dist_sq <= (quads[quad_ix].edge_length + thresh_dilation)
                        && dist_sq <= (quads[other_ix].edge_length + thresh_dilation)
                    {
                        let this_q = &quads[quad_ix];
                        let other_q = &quads[other_ix];

                        let same_quadrant = check_shared_corner_quadrant(
                            this_q,
                            corner_this_ix,
                            other_q,
                            corner_other_ix,
                        );
                        if same_quadrant {
                            closest_corner_ix = Some(corner_other_ix);
                            closest_quad_ix = Some(other_ix);
                            min_distance = dist_sq;
                        }
                    }
                }
            }
            if let Some(closest_quad_ix) = closest_quad_ix {
                debug_assert!(min_distance < f32::MAX);

                let closest_corner_ix = closest_corner_ix.unwrap();

                // Check whether we are already neighbour of this quad.
                if quads[closest_quad_ix].neighbors_ix.contains(&Some(quad_ix)) {
                    continue 'quad_loop;
                }

                let mut closest_corner = quads[closest_quad_ix].corners[closest_corner_ix].clone();
                closest_corner.pos = na::center(&closest_corner.pos, &pt);

                let closest_quad = &mut quads[closest_quad_ix];
                closest_quad.neighbors_ix[closest_corner_ix] = Some(quad_ix);
                closest_quad.corners[closest_corner_ix] = closest_corner.clone();

                let quad = &mut quads[quad_ix];
                quad.neighbors_ix[corner_this_ix] = Some(closest_quad_ix);
                quad.corners[corner_this_ix] = closest_corner.clone();
            }
        }
    }
}
#[derive(Debug, Clone)]
enum AugmentationResult {
    SUCCESS,
    FAILURE,
}
fn augment_best_run(
    quads: &mut Vec<QuadrangleCandidate>,
    old_quad_group: &mut Vec<usize>,
    new_quads: &mut [QuadrangleCandidate],
    old_dilation: u32,
    new_dilation: u32,
) -> AugmentationResult {
    let thresh_dilation = ((2 * old_dilation + 3) * (2 * new_dilation + 3) * 2) as f32;

    for ix in 0..old_quad_group.len() {
        let quad_ix = old_quad_group[ix];
        for corner_this_ix in 0..4 {
            let mut min_distance = f32::MAX;
            let mut closest_quad_ix: Option<usize> = None;
            let mut closest_corner_idx: Option<usize> = None;

            if !quads[quad_ix].corners[corner_this_ix].needs_neighbour {
                continue;
            }

            let pt = quads[quad_ix].corners[corner_this_ix].pos;

            // Find the overall closest corner.
            for other_quad in new_quads.iter() {
                if other_quad.labeled {
                    continue;
                }
                for corner_other_ix in 0..4 {
                    if other_quad.neighbors_ix[corner_other_ix].is_some() {
                        continue;
                    }
                    let corner_other = &other_quad.corners[corner_other_ix];

                    let dx = pt.x - corner_other.pos.x;
                    let dy = pt.y - corner_other.pos.y;
                    let dist_sq = dx * dx + dy * dy;

                    if dist_sq < min_distance
                        && dist_sq <= (quads[quad_ix].edge_length + thresh_dilation)
                        && dist_sq <= (other_quad.edge_length + thresh_dilation)
                    {
                        let this_q = &quads[quad_ix];

                        let same_quadrant = check_shared_corner_quadrant(
                            this_q,
                            corner_this_ix,
                            other_quad,
                            corner_other_ix,
                        );
                        if same_quadrant {
                            closest_corner_idx = Some(corner_other_ix);
                            closest_quad_ix = Some(corner_other_ix);
                            min_distance = dist_sq;
                        }
                    }
                }
            }

            if min_distance < f32::MAX {
                if let Some(closest_quad_ix) = closest_quad_ix {
                    // Index of the quad we will push further down.
                    let new_quad_ix = quads.len();

                    let closest_quad = &mut new_quads[closest_quad_ix];

                    let closest_corner = &mut closest_quad.corners[closest_corner_idx.unwrap()];
                    closest_corner.pos = na::center(&closest_corner.pos, &pt);
                    closest_quad.labeled = true;

                    let quad: &mut QuadrangleCandidate = &mut quads[quad_ix];
                    quad.corners[corner_this_ix].pos = closest_corner.pos;
                    quad.neighbors_ix[corner_this_ix] = Some(new_quad_ix);

                    let new_quad = QuadrangleCandidate {
                        group: quad.group,
                        edge_length: closest_quad.edge_length,
                        corners: closest_quad.corners.clone(),
                        neighbors_ix: [Some(quad_ix), None, None, None],
                        labeled: false,
                    };

                    quads.push(new_quad);
                    old_quad_group.push(new_quad_ix);

                    return AugmentationResult::SUCCESS;
                }
            }
        }
    }
    AugmentationResult::FAILURE
}

fn find_connected_quads(
    quads: &mut Vec<QuadrangleCandidate>,
    group_ix: u32,
    _dilation: u32,
) -> Vec<usize> {
    let first_unlabeled_ix = quads
        .iter()
        .position(|quad| (quad.neighbor_count() > 0) && quad.group.is_none());

    let mut stack = Vec::default();
    let mut out_group = Vec::default();

    if let Some(first_unlabeled_ix) = first_unlabeled_ix {
        stack.push(first_unlabeled_ix);
        out_group.push(first_unlabeled_ix);
        quads[first_unlabeled_ix].group = Some(group_ix);

        while !stack.is_empty() {
            let quad_ix = stack.pop().unwrap();

            quads[quad_ix]
                .neighbors_ix
                .clone()
                .iter()
                .filter_map(|i| *i)
                .for_each(|nq_ix| {
                    let nq = &mut quads[nq_ix];
                    if nq.neighbor_count() > 0 && nq.group.is_none() {
                        nq.group = Some(group_ix);
                        stack.push(nq_ix);
                        out_group.push(nq_ix);
                    }
                })
        }
    }
    out_group
}

#[derive(Debug, Clone)]
struct GridExtend {
    min_y: i32,
    max_y: i32,
    min_x: i32,
    max_x: i32,
}

fn compute_extent(quads: &mut dyn Iterator<Item = &QuadrangleCandidate>) -> GridExtend {
    let mut min_y = i32::MAX;
    let mut max_y = i32::MIN;
    let mut min_x = i32::MAX;
    let mut max_x = i32::MIN;

    quads.flat_map(|q| q.corners.iter()).for_each(|corner| {
        max_y = max_y.max(corner.ix.y);
        min_y = min_y.min(corner.ix.y);

        max_x = max_x.max(corner.ix.x);
        min_x = min_x.min(corner.ix.x);
    });

    GridExtend {
        min_y,
        max_y,
        min_x,
        max_x,
    }
}

fn for_each_group_corner(
    quads: &mut [QuadrangleCandidate],
    group: &[usize],
    f: &dyn Fn(&mut CornerCandidate),
) {
    for &ix in group {
        let q = &mut quads[ix];
        for corner in q.corners.iter_mut() {
            f(corner)
        }
    }
}

/// Label each corner with their correct x/y index within the grid.
#[allow(clippy::cognitive_complexity)]
fn label_quad_group(
    quads: &mut [QuadrangleCandidate],
    group: &[usize],
    checker_spec: &CheckerboardSpecification,
    first_run: bool,
) {
    if first_run {
        // Look for the first quad with 4 neighbours.
        let mut max_ix = {
            group
                .iter()
                .map(|&qix| quads[qix].neighbor_count())
                .position(|value| value == 4)
        };
        // Otherwise take the one with the most neighbors.
        if max_ix.is_none() {
            max_ix = group
                .iter()
                .max_by_key(|&&qix| quads[qix].neighbor_count())
                .cloned();
        };
        let max_ix = group[max_ix.expect("Could not locate a starting quad.")];

        // Mark this as the quad at 0,0
        let q = &mut quads[max_ix];
        q.corners[0].ix = na::Point2::new(0, 0);
        q.corners[1].ix = na::Point2::new(0, 1);
        q.corners[2].ix = na::Point2::new(1, 1);
        q.corners[3].ix = na::Point2::new(1, 0);

        q.labeled = true;
    }

    let mut flag_changed = true;
    while flag_changed {
        flag_changed = false;

        for &quad_ix in group.iter().rev() {
            if quads[quad_ix].labeled {
                continue;
            }

            for (local_neighbor_ix, quad_neighbor_ix) in
                quads[quad_ix].neighbors_ix.iter().enumerate()
            {
                if quad_neighbor_ix.is_none() {
                    continue;
                }
                let quad_neighbor_ix = quad_neighbor_ix.unwrap();
                let quad_neighbor = &quads[quad_neighbor_ix];
                if !quad_neighbor.labeled {
                    continue;
                }
                let cc_ix = quad_neighbor
                    .neighbors_ix
                    .iter()
                    .position(|&other| other.is_some() && other.unwrap() == quad_ix);

                if cc_ix.is_none() {
                    // Somehow the link got lost. Maybe the other corner got paired up with someone else?
                    eprintln!("Violated assumption that neighbour relationships are symmetric.");
                    continue;
                }
                let cc_ix = cc_ix.unwrap();

                let ccn = [
                    quad_neighbor.corners[cc_ix].ix.coords,
                    quad_neighbor.corners[(cc_ix + 1) % 4].ix.coords,
                    quad_neighbor.corners[(cc_ix + 2) % 4].ix.coords,
                    quad_neighbor.corners[(cc_ix + 3) % 4].ix.coords,
                ];

                let corners = &mut quads[quad_ix].corners;

                corners[local_neighbor_ix].ix = ccn[0].into();
                corners[(local_neighbor_ix + 1) % 4].ix = (ccn[0] - ccn[2] + ccn[3]).into();
                corners[(local_neighbor_ix + 2) % 4].ix = (ccn[0] + ccn[0] - ccn[2]).into();
                corners[(local_neighbor_ix + 3) % 4].ix = (ccn[0] - ccn[2] + ccn[1]).into();

                quads[quad_ix].labeled = true;

                flag_changed = true;
                break;
            }
        }
    }

    let extent = compute_extent(&mut group.iter().map(|&ix| &quads[ix]));
    let min_xy = na::Vector2::new(extent.min_x, extent.min_y);

    // Normalise the corner indices
    for_each_group_corner(quads, group, &|corner: &mut CornerCandidate| {
        corner.ix -= min_xy;
    });

    let extent = compute_extent(&mut group.iter().map(|&ix| &quads[ix]));
    let min_y = extent.min_y;
    let max_y = extent.max_y;
    let min_x = extent.min_x;
    let max_x = extent.max_x;
    debug_assert_eq!(min_x, 0);
    debug_assert_eq!(min_y, 0);

    for i in min_y..max_y {
        for j in min_x..max_x {
            let mut other = None;

            for quad_group_ix in 0..group.len() {
                for c_ix in 0..4 {
                    let corner = &mut quads[group[quad_group_ix]].corners[c_ix];
                    if corner.ix.y == i && corner.ix.x == j {
                        match other {
                            None => {
                                corner.needs_neighbour = true;
                                other = Some((group[quad_group_ix], c_ix));
                            }
                            Some((quad_id, corner_id)) => {
                                corner.needs_neighbour = false;
                                quads[quad_id].corners[corner_id].needs_neighbour = false
                            }
                        }
                    }
                }
            }
        }
    }
    for i in min_y..max_y {
        for j in min_x..max_x {
            let mut other = None;

            for &quad_ix in group {
                for c_ix in 0..4 {
                    if quads[quad_ix].corners[c_ix].ix.y == i
                        && quads[quad_ix].corners[c_ix].ix.x == j
                    {
                        match other {
                            None => {
                                other = Some((quad_ix, c_ix));
                            }
                            Some((other_quad_ix, other_corner_ix)) => {
                                // TODO implement warning for multiple visit
                                let (delta, interp) = {
                                    let corner = &quads[quad_ix].corners[c_ix];
                                    (
                                        corner.pos
                                            - quads[other_quad_ix].corners[other_corner_ix].pos,
                                        (0.5_f32
                                            * (corner.pos.coords
                                                + quads[other_quad_ix].corners[other_corner_ix]
                                                    .pos
                                                    .coords))
                                            .into(),
                                    )
                                };

                                if delta.x != 0.0 || delta.y != 0.0 {
                                    quads[quad_ix].corners[c_ix].pos = interp;
                                    quads[other_quad_ix].corners[other_corner_ix].pos = interp;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    let larger_dim_pattern = checker_spec.height.max(checker_spec.width) as i32;
    let smaller_dim_pattern = checker_spec.height.min(checker_spec.width) as i32;
    let mut flag_smaller_dim1 = false;
    let mut flag_smaller_dim2 = false;

    if (larger_dim_pattern + 1) == (max_x - min_x) {
        flag_smaller_dim1 = true;
        for_each_group_corner(quads, group, &|corner: &mut CornerCandidate| {
            if corner.ix.x == min_x || corner.ix.x == max_x {
                corner.needs_neighbour = false;
            }
        })
    }

    if larger_dim_pattern + 1 == max_y - min_y {
        flag_smaller_dim2 = true;
        for_each_group_corner(quads, group, &|corner: &mut CornerCandidate| {
            if corner.ix.y == min_y || corner.ix.y == max_y {
                corner.needs_neighbour = false;
            }
        })
    }

    match (flag_smaller_dim1, flag_smaller_dim2) {
        (false, true) => {
            if smaller_dim_pattern + 1 == max_x - min_x {
                for_each_group_corner(quads, group, &|corner: &mut CornerCandidate| {
                    if corner.ix.x == min_x || corner.ix.x == max_x {
                        corner.needs_neighbour = false;
                    }
                })
            }
        }
        (true, false) => {
            if smaller_dim_pattern + 1 == max_y - min_y {
                for_each_group_corner(quads, group, &|corner: &mut CornerCandidate| {
                    if corner.ix.y == min_y || corner.ix.y == max_y {
                        corner.needs_neighbour = false;
                    };
                });
            }
        }
        (false, false) if smaller_dim_pattern + 1 < max_x - min_x => {
            if smaller_dim_pattern + 1 == max_y - min_y {
                for_each_group_corner(quads, group, &|corner: &mut CornerCandidate| {
                    if corner.ix.y == min_y || corner.ix.y == max_y {
                        corner.needs_neighbour = false;
                    }
                })
            }
        }
        (false, false) if smaller_dim_pattern + 1 < max_y - min_y => {
            if smaller_dim_pattern + 1 == max_x - min_x {
                for_each_group_corner(quads, group, &|corner: &mut CornerCandidate| {
                    if corner.ix.x == min_x || corner.ix.x == max_x {
                        corner.needs_neighbour = false;
                    }
                })
            }
        }
        _ => {}
    }
}

fn clean_found_connected_quads(
    quads: &mut [QuadrangleCandidate],
    quad_group: &mut Vec<usize>,
    spec: &CheckerboardSpecification,
) {
    let mut centers: Vec<Vec2f> = Vec::with_capacity(quad_group.len());

    let mut center: Vec2f = [0.0, 0.0].into();

    let expected = spec.quad_count();

    if (quad_group.len() as u32) <= expected {
        return;
    }

    for &q_ix in quad_group.iter() {
        let quad = &quads[q_ix];
        let mut ci: Vec2f = [0.0, 0.0].into();
        for corner in &quad.corners {
            ci[0] += corner.pos.x;
            ci[1] += corner.pos.y;
        }
        debug_assert_eq!(quad.corners.len(), 4);

        ci[0] *= 0.25;
        ci[1] *= 0.25;
        center[0] += ci[0];
        center[1] += ci[1];
        centers.push(ci);
    }

    center[0] /= quad_group.len() as f32;
    center[1] /= quad_group.len() as f32;

    // Reduce bounding box until we have a sensible num,ber of rectangles
    while quad_group.len() as u32 > expected {
        let mut min_box_area = f32::MAX;
        let mut min_box_area_ix: Option<usize> = None;

        for skip in 0..quad_group.len() {
            // Remove the skip center and temporarily replace it with the overall center.
            let tmp = centers[skip];
            centers[skip] = center;

            // Compute convex hull
            let point_mat = Mat::from_slice(&centers).unwrap();
            let mut hull = Mat::default();
            convex_hull(&point_mat, &mut hull, false, true).unwrap();
            let hull_area = contour_area(&hull, false).unwrap() as f32;

            // Return skipped quad center
            centers[skip] = tmp;

            if hull_area < min_box_area {
                min_box_area = hull_area;
                min_box_area_ix = Some(skip);
            }
        }

        let q0 = quad_group[min_box_area_ix.unwrap()];
        // Remove q0 from any quads that have it as neighbor.
        {
            let mut removed: Vec<usize> = Vec::with_capacity(4);

            for &q_ix in quad_group.iter() {
                let q = &mut quads[q_ix];
                if let Some(ix) = q
                    .neighbors_ix
                    .iter()
                    .position(|&item| item.is_some() && item.unwrap() == q0)
                {
                    q.neighbors_ix[ix] = None;
                    removed.push(q_ix);
                };
            }
            // Remove all quads that had q0 removed also from q0.
            for &removed_ix in &removed {
                if let Some(ix) = quads[q0]
                    .neighbors_ix
                    .iter()
                    .position(|&item| item.is_some() && item.unwrap() == removed_ix)
                {
                    quads[q0].neighbors_ix[ix] = None;
                };
            }
        }

        let q0_group_ix = quad_group.iter().position(|&item| item == q0).unwrap();
        quad_group.remove(q0_group_ix);
        centers.remove(q0_group_ix);
        debug_assert_eq!(quad_group.len(), centers.len());
    }
}
#[derive(Debug, Clone)]
enum GridValidationResult {
    FullGrid,
    PartialGrid,
    Error(&'static str),
}

fn validate_grid(
    quads: &[QuadrangleCandidate],
    output_quads: &[usize],
    spec: &CheckerboardSpecification,
) -> GridValidationResult {
    if output_quads.is_empty() {
        return GridValidationResult::Error("No quads provided");
    }

    let mut flag_y = false;
    let mut flag_x = false;
    let max_pattern_size_row: i32;
    let max_pattern_size_column: i32;

    let extent = compute_extent(&mut output_quads.iter().map(|&ix| &quads[ix]));
    let min_y = extent.min_y;
    let max_y = extent.max_y;
    let min_x = extent.min_x;
    let max_x = extent.max_x;

    for &q_ix in output_quads {
        let q: &QuadrangleCandidate = &quads[q_ix];

        for corner in &q.corners {
            let ix_x = corner.ix.x;
            let ix_y = corner.ix.y;

            if !corner.needs_neighbour {
                if ix_x == max_x && ix_y != min_y && ix_y != max_y {
                    flag_x = true
                }
                if ix_y == max_y && ix_x != min_x && ix_x != max_x {
                    flag_y = true
                }
            }
        }
    }

    if flag_x {
        if max_x - min_x != (spec.width as i32 + 1) {
            max_pattern_size_column = spec.height as i32;
            max_pattern_size_row = spec.width as i32;
        } else {
            max_pattern_size_column = spec.width as i32;
            max_pattern_size_row = spec.height as i32;
        }
    } else if flag_y {
        if max_y - min_y == (spec.width as i32 + 1) {
            max_pattern_size_row = spec.width as i32;
            max_pattern_size_column = spec.height as i32;
        } else {
            max_pattern_size_row = spec.height as i32;
            max_pattern_size_column = spec.width as i32;
        }
    } else {
        max_pattern_size_column = spec.width.max(spec.height) as i32;
        max_pattern_size_row = spec.width.max(spec.height) as i32;
    }

    let mut corner_count = 0;

    for i in (min_y + 1)..=(max_pattern_size_row + min_y) {
        for j in (min_x + 1)..=(max_pattern_size_column + min_x) {
            // Reset the iterator
            let mut iter = 1;

            for &q_ix in output_quads {
                let q: &QuadrangleCandidate = &quads[q_ix];
                for corner in &q.corners {
                    let ix = corner.ix;

                    if (ix.y == i) && (ix.x == j) {
                        if iter == 2 {
                            corner_count += 1;
                        }

                        if iter > 2 {
                            return GridValidationResult::Error("Corner linking error occurred.");
                        }
                        iter += 1;
                    }
                }
            }
        }
    }

    if corner_count >= spec.interior_corner_count() {
        GridValidationResult::FullGrid
    } else {
        GridValidationResult::PartialGrid
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use assert_approx_eq::assert_approx_eq;
    use std::collections::HashMap;

    #[test]
    fn detect_simple_image() {
        let spec = CheckerboardSpecification {
            width: 22,
            height: 12,
        };
        let image = image::open(&"assets/reference_easy/checker2212.png")
            .unwrap()
            .to_rgb();

        let checkerboard =
            detect_checkerboard(&image, &spec).expect("Did not receive a valid checkerboard.");

        let corners = checkerboard.get_sorted_corners();

        assert_eq!(
            corners.len() as u32,
            297,
            "Wrong number of corners detected."
        );

        let corners_indexed: HashMap<[i32; 2], Corner> =
            corners.into_iter().map(|c| (c.ix, c)).collect();

        // Note: data generated by originals paper's binary.
        let x = [
            [
                797.5, 933.5, 1069.5, 1207.5, 1343.5, 1479.5, 1615.5, 1751.5, 1887.5, 2023.5,
                2159.5, 2297.5, 2433.5, 2569.5, 2705.5, 2841.5, 2977.5, 3113.5, 3251.5, 3387.5,
                3523.5,
            ],
            [
                797.5, 933.5, 1069.5, 1207.5, 1343.5, 1479.5, 1615.5, 1751.5, 1887.5, 2023.5,
                2159.5, 2297.5, 2433.5, 2569.5, 2705.5, 2841.5, 2977.5, 3113.5, 3251.5, 3387.5,
                3523.5,
            ],
            [
                797.5, 933.5, 1069.5, 1207.5, 1343.5, 1479.5, 1615.5, 1751.5, 1887.5, 2023.5,
                2159.5, 2297.5, 2433.5, 2569.5, 2705.5, 2841.5, 2977.5, 3113.5, 3251.5, 3387.5,
                3523.5,
            ],
            [
                797.5, 933.5, 1069.5, 1207.5, 1343.5, 1479.5, 1615.5, 1751.5, 1887.5, 2023.5,
                2159.5, 2297.5, 2433.5, 2569.5, 2705.5, 2841.5, 2977.5, 3113.5, 3251.5, 3387.5,
                3523.5,
            ],
            [
                797.5, 933.5, 1069.5, 1207.5, 1343.5, 1479.5, 1615.5, 1751.5, 1887.5, 2023.5,
                2159.5, 2297.5, 2433.5, 2569.5, 2705.5, 2841.5, 2977.5, 3113.5, 3251.5, 3387.5,
                3523.5,
            ],
            [
                797.5, 933.5, 1069.5, 1207.5, 1343.5, 1479.5, 1615.5, 1751.5, 1887.5, 2023.5,
                2159.5, 2297.5, 2433.5, 2569.5, 2705.5, 2841.5, 2977.5, 3113.5, 3251.5, 3387.5,
                3523.5,
            ],
            [
                797.5, 933.5, 1069.5, 1207.5, 1343.5, 1479.5, 1615.5, 1751.5, 1887.5, 2023.5,
                2159.5, 2297.5, 2433.5, 2569.5, 2705.5, 2841.5, 2977.5, 3113.5, 3251.5, 3387.5,
                3523.5,
            ],
            [
                797.5, 933.5, 1069.5, 1207.5, 1343.5, 1479.5, 1615.5, 1751.5, 1887.5, 2023.5,
                2159.5, 2297.5, 2433.5, 2569.5, 2705.5, 2841.5, 2977.5, 3113.5, 3251.5, 3387.5,
                3523.5,
            ],
            [
                797.5, 933.5, 1069.5, 1207.5, 1343.5, 1479.5, 1615.5, 1751.5, 1887.5, 2023.5,
                2159.5, 2297.5, 2433.5, 2569.5, 2705.5, 2841.5, 2977.5, 3113.5, 3251.5, 3387.5,
                3523.5,
            ],
            [
                797.5, 933.5, 1069.5, 1207.5, 1343.5, 1479.5, 1615.5, 1751.5, 1887.5, 2023.5,
                2159.5, 2297.5, 2433.5, 2569.5, 2705.5, 2841.5, 2977.5, 3113.5, 3251.5, 3387.5,
                3523.5,
            ],
            [
                797.5, 933.5, 1069.5, 1207.5, 1343.5, 1479.5, 1615.5, 1751.5, 1887.5, 2023.5,
                2159.5, 2297.5, 2433.5, 2569.5, 2705.5, 2841.5, 2977.5, 3113.5, 3251.5, 3387.5,
                3523.5,
            ],
        ];

        let y = [
            [
                607.5, 607.5, 607.5, 607.5, 607.5, 607.5, 607.5, 607.5, 607.5, 607.5, 607.5, 607.5,
                607.5, 607.5, 607.5, 607.5, 607.5, 607.5, 607.5, 607.5, 607.5,
            ],
            [
                775.5, 775.5, 775.5, 775.5, 775.5, 775.5, 775.5, 775.5, 775.5, 775.5, 775.5, 775.5,
                775.5, 775.5, 775.5, 775.5, 775.5, 775.5, 775.5, 775.5, 775.5,
            ],
            [
                941.5, 941.5, 941.5, 941.5, 941.5, 941.5, 941.5, 941.5, 941.5, 941.5, 941.5, 941.5,
                941.5, 941.5, 941.5, 941.5, 941.5, 941.5, 941.5, 941.5, 941.5,
            ],
            [
                1107.5, 1107.5, 1107.5, 1107.5, 1107.5, 1107.5, 1107.5, 1107.5, 1107.5, 1107.5,
                1107.5, 1107.5, 1107.5, 1107.5, 1107.5, 1107.5, 1107.5, 1107.5, 1107.5, 1107.5,
                1107.5,
            ],
            [
                1273.5, 1273.5, 1273.5, 1273.5, 1273.5, 1273.5, 1273.5, 1273.5, 1273.5, 1273.5,
                1273.5, 1273.5, 1273.5, 1273.5, 1273.5, 1273.5, 1273.5, 1273.5, 1273.5, 1273.5,
                1273.5,
            ],
            [
                1441.5, 1441.5, 1441.5, 1441.5, 1441.5, 1441.5, 1441.5, 1441.5, 1441.5, 1441.5,
                1441.5, 1441.5, 1441.5, 1441.5, 1441.5, 1441.5, 1441.5, 1441.5, 1441.5, 1441.5,
                1441.5,
            ],
            [
                1607.5, 1607.5, 1607.5, 1607.5, 1607.5, 1607.5, 1607.5, 1607.5, 1607.5, 1607.5,
                1607.5, 1607.5, 1607.5, 1607.5, 1607.5, 1607.5, 1607.5, 1607.5, 1607.5, 1607.5,
                1607.5,
            ],
            [
                1773.5, 1773.5, 1773.5, 1773.5, 1773.5, 1773.5, 1773.5, 1773.5, 1773.5, 1773.5,
                1773.5, 1773.5, 1773.5, 1773.5, 1773.5, 1773.5, 1773.5, 1773.5, 1773.5, 1773.5,
                1773.5,
            ],
            [
                1939.5, 1939.5, 1939.5, 1939.5, 1939.5, 1939.5, 1939.5, 1939.5, 1939.5, 1939.5,
                1939.5, 1939.5, 1939.5, 1939.5, 1939.5, 1939.5, 1939.5, 1939.5, 1939.5, 1939.5,
                1939.5,
            ],
            [
                2107.5, 2107.5, 2107.5, 2107.5, 2107.5, 2107.5, 2107.5, 2107.5, 2107.5, 2107.5,
                2107.5, 2107.5, 2107.5, 2107.5, 2107.5, 2107.5, 2107.5, 2107.5, 2107.5, 2107.5,
                2107.5,
            ],
            [
                2273.5, 2273.5, 2273.5, 2273.5, 2273.5, 2273.5, 2273.5, 2273.5, 2273.5, 2273.5,
                2273.5, 2273.5, 2273.5, 2273.5, 2273.5, 2273.5, 2273.5, 2273.5, 2273.5, 2273.5,
                2273.5,
            ],
        ];

        // The original Rufli algorithm only returns the inner grid. So that is what we compare here.
        for xi in 0..19 {
            for yi in 0..9 {
                let xy_reference = [x[yi][xi] as f32, y[yi][xi] as f32];

                let xy_observed = corners_indexed[&[1 + yi as i32, 1 + xi as i32]].pos;
                assert_approx_eq!(xy_reference[0], xy_observed[0]);
                assert_approx_eq!(xy_reference[1], xy_observed[1]);
            }
        }
    }
}
