#![deny(
    missing_docs,
    missing_debug_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unsafe_code,
    unstable_features,
    unused_import_braces,
    unused_qualifications
)]
/*!
# checkerboard-rs

Checkerboard detection algorithms implemented in rust.

Right now this library only exposes a re-implementation of the algorithm described in [1, 2, 3]

1. Scaramuzza, D., Martinelli, A. and Siegwart, R.. "A Flexible Technique for Accurate Omnidirectional Camera Calibration and Structure from Motion", Proceedings of IEEE International Conference of Vision Systems (ICVS'06), New York, January 5-7, 2006.
2. Scaramuzza, D., Martinelli, A. and Siegwart, R.. "A Toolbox for Easy Calibrating Omnidirectional Cameras", Proceedings to IEEE International Conference on Intelligent Robots and Systems (IROS 2006), Beijing China, October 7-15, 2006.
3. Rufli, M., Scaramuzza, D., and Siegwart, R., Automatic Detection of Checkerboards on Blurred and Distorted Images,Proceedings of the IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS 2008), Nice, France, September 2008.

The API is still unstable and will most likely evolve with more use (by me or through external feedback).

*/
pub mod rufli;

/// Represents a checkerboard and contains its corner and quadrangle information.
#[derive(Debug, Clone)]
pub struct Checkerboard {
    corners: Vec<Corner>,
    spec: CheckerboardSpecification,
}

impl Checkerboard {
    /// Returns a copy of all corners in the checkerboard.
    /// Corners are sorted ascending by their index.
    pub fn get_sorted_corners(&self) -> Vec<Corner> {
        let mut corners = self.corners.clone();
        corners.sort_by_key(|item| item.ix);
        corners
    }
}

/// A single corner of a checkerboard.
#[derive(Debug, Clone)]
pub struct Corner {
    /// Index of the corner within the checkerboard.
    pub ix: [i32; 2],
    /// Position of the corner within the detection frame.
    pub pos: [f32; 2],
}

/// CheckerboardSpecification describes the properties of a checkerboard.
#[derive(Debug, Clone)]
pub struct CheckerboardSpecification {
    /// Width of the checkerboard in checkers.
    pub width: u32,
    /// Height of the checkerboard in checkers.
    pub height: u32,
}

impl CheckerboardSpecification {
    /// Number of quadrangles in the specified Checkerboard.
    pub fn quad_count(&self) -> u32 {
        ((self.width + 1) * (self.height + 1)) / 2
    }

    pub(crate) fn interior_corner_count(&self) -> u32 {
        (self.width - 1) * (self.height - 1)
    }
}
