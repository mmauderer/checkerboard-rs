# checkerboard-rs

Checkerboard detection algorithms implemented in rust.

Right now this library only exposes a re-implementation of the algorithm described in [1, 2, 3]

1. Scaramuzza, D., Martinelli, A. and Siegwart, R.. "A Flexible Technique for Accurate Omnidirectional Camera Calibration and Structure from Motion", Proceedings of IEEE International Conference of Vision Systems (ICVS'06), New York, January 5-7, 2006.
2. Scaramuzza, D., Martinelli, A. and Siegwart, R.. "A Toolbox for Easy Calibrating Omnidirectional Cameras", Proceedings to IEEE International Conference on Intelligent Robots and Systems (IROS 2006), Beijing China, October 7-15, 2006. 
3. Rufli, M., Scaramuzza, D., and Siegwart, R., Automatic Detection of Checkerboards on Blurred and Distorted Images,Proceedings of the IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS 2008), Nice, France, September 2008.

The API is still unstable and will most likely evolve with more use (by me or through external feedback).

## Getting Started

This library currently requires OpenCV 3.2 and uses [opencv-rust](https://github.com/twistedfall/opencv-rust) for bindings.
Please see the their repository on how to set up OpenCV to get opencv-rust to build.

## Example

```$rust
use checkerboard_rs::CheckerboardSpecification;
use checkerboard_rs::rufli::detect_checkerboard;

let spec = CheckerboardSpecification {
    width: 22,
    height: 12,
};
let image = image::open(&"assets/reference_easy/checker2212.png")
    .unwrap()
    .to_rgb();

let quads =
    detect_checkerboard(&image, &spec).expect("Did not receive a valid checkerboard.");
```

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.